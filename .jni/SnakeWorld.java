import greenfoot.*;

/**
 * @author (Francisco Guerra) 
 * @version (Version 1)
 */
public class SnakeWorld extends World {

	public SnakeWorld() {
    	super(width(), height(), cellSize());
		start();
	}
    
    public void start(){
        start_();
    }
    private native void start_();

    public Snake getSnake() {
        return getSnake_();
    }
    private native Snake getSnake_();

    private native static int width();
    private native static int height();
    private native static int cellSize();
    
    static {
        System.load(new java.io.File(".jni", "SnakeWorld_jni.so").getAbsolutePath());
    }
}
