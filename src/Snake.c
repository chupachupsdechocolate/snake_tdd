/**
 * The Snake.c file defines a game actor that has a 50x40 size image built 
 * from the "snake.png" file. The actSnake() method declared in the Snake.c  
 * file defines the snake behaviour in each cycle of the scenario execution.
 * 
 * @author (Francisco Guerra) 
 * @version (Version 1)
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "greenfoot.h"

/**
 * Initialize its image.
 */
void startSnake(Actor snake) {
	setImageFile(snake, "snake.png");
	setImageScale(snake, 45, 45);
}

void actSnake(Actor snake) {

}

void setAngle(Actor snake, int angle) {
    
}

void setDistance(Actor snake, int distance) {
    
}
